const Lsi = {
  Admin: {
    usersList: { en: "Users List" },
    parkingPlaces: { en: "Parking Places" },
    reservations: { en: "Reservations" },
    weeklyOverview: { en: "Weekly Overview" },
    settings: { en: "Settings" },
  },
};

export default Lsi;
