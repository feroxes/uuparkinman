const Lsi = {
  appName: {
    cs: "Aplikace uuPlaces",
    en: "Application uuPlaces",
  },

  left: {
    home: {
      cs: "Vítejte",
      en: "Welcome",
    },
    about: {
      cs: "O aplikaci",
      en: "About Application",
    },
  },

  about: {
    header: {
      en: "uuPlaces",
    },
    creatorsHeader: {
      en: "Application creators",
    },
  },

  auth: {
    welcome: {
      en: "Hello and Welcome, my dear friend!",
    },
  },

  unauth: {
    continueToMain: {
      cs: "Pokračovat na web produktu",
      en: "Continue to the product web",
    },
    notAuthorized: {
      cs: "Nemáte dostatečná práva k použití aplikace",
      en: "You do not have sufficient rights to use the application",
    },
  },

  unauthInit: {
    buyYourOwn: {
      cs: "Můžete si koupit vlastní uuPlaces.",
      en: "You can buy your own uuPlaces.",
    },
    notAuthorized: {
      cs: "Nemáte právo inicializovat tuto aplikaci uuPlaces.",
      en: "You don't have rights to initialize this uuPlaces.",
    },
  },

  controlPanel: {
    rightsError: {
      cs: "K zobrazení komponenty nemáte dostatečná práva.",
      en: "You do not have sufficient rights to display this component.",
    },

    btNotConnected: {
      cs: "Aplikace není napojená na Business Territory",
      en: "The application is not connected to a Business Territory",
    },
  },
};

export default Lsi;
