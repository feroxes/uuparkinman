export const About = {
  about: {
    en: "Enjoy 🤙",
  },
  licence: {
    organisation: {
      en: {
        name: "Unicorn a.s.",
        uri: "https://www.unicorn.com/",
      },
    },
    authorities: {
      en: [{ name: "Yarik Harmash" }, { name: "Roman Hurzhii" }],
    },
  },
  leadingAuthors: [
    {
      name: "Yarik Harmash",
      uuIdentity: "8517-626-1",
      role: {
        en: "Head of Development",
      },
    },
    {
      name: "Roman Hurhii",
      uuIdentity: "12-1350-1",
      role: {
        en: "Operation Manager, Chief Developer",
      },
    },
  ],
};

export default About;
