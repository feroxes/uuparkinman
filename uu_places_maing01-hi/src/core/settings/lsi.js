const Lsi = {
  dayOfStartReservations: { en: "Day of reservation start" },
  hourOfStartReservations: { en: "Hour of reservation start" },
  message: { en: "Message" },
  formHeader: { en: "Select reservation settings" },
  sendMessageFormHeader: { en: "Send message to telegram" },
  success: { en: "Application has been successfully updated" },
  successMessageSend: { en: "Message has been successfully sent" },
};
export default Lsi;
