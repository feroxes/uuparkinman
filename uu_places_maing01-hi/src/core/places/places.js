//@@viewOn:imports
import { PlacesContext } from "./context/context.js";
import { usePlaces } from "./context/context.js";
import { Loader } from "./places-loader.js";
//@@viewOff:imports

const Places = {
  PlacesContext,
  usePlaces,
  Loader,
};

export { PlacesContext, usePlaces, Loader };
export default Places;
